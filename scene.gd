extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const BRICK = preload("res://scenes/green_brick.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	for y in range(5, 9):
		var newPos = Vector2(0, y * 20)
		for x in range(2, 26):
			newPos.x = x * 38
			var brick = BRICK.instance()
			brick.position = newPos
			add_child(brick)