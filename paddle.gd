extends StaticBody2D

export (int) var movement_speed = 300.0

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	get_input(delta)
	
	if position.x < 20.0:
		position.x = 20
	if position.x > 1000:
		position.x = 1000
	
	
func get_input(delta):
	
	var input_dir = Vector2()
	if Input.is_action_pressed("ui_left"):
		input_dir.x -= 1.0
		
	if Input.is_action_pressed("ui_right"):
		input_dir.x += 1.0

	position += (delta * movement_speed * input_dir)