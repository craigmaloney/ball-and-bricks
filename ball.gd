extends RigidBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var collisions = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	linear_velocity.x = rand_range(-100, 100)

func _process(delta):
	if collisions > 5:
		linear_velocity *= 1.5
		collisions = 0

func _on_ball_body_entered(body):
	if body.is_in_group("brick"):
		body.queue_free()
		collisions += 1
		get_node("doot").play()
	if body.is_in_group("wall"):
		get_node("doot").play()
	if body.is_in_group("paddle"):
		get_node("doot").play()
	if body.is_in_group("floor"):
		get_node("ugh").play()
		get_node("lost_ball").start()
		linear_velocity.x = 0
		linear_velocity.y = 0